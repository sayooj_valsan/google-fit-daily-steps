package tracker.hive.com;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.fitness.result.DataReadResult;
import org.intelligentsia.hirondelle.date4j.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import tracker.hive.com.helper.GoogleFitManager;
import utils.Constants;

/**
 * Responsible for reading daily steps
 */
public class ReadDailyStepsService extends IntentService {
    private static final String ACTION_READ_DAILY_STEPS = "ACTION_READ_DAILY_STEPS";
    private static final String TAG = ReadDailyStepsService.class.getSimpleName();

    public static void readyDailySteps(Context context) {
        Intent intent = new Intent(context, ReadDailyStepsService.class);
        intent.setAction(ACTION_READ_DAILY_STEPS);
        context.startService(intent);
    }

    public ReadDailyStepsService() {
        super("ReadDailyStepsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_READ_DAILY_STEPS.equals(action)) {
                readDailySteps();
            }
        }
    }


    private void readDailySteps() {
        if(GoogleFitManager.getInstance().getGoogleApiClient() == null){
            return;
        }
        DailyTotalResult dataReadResult =
        Fitness.HistoryApi.readDailyTotal(GoogleFitManager.getInstance().getGoogleApiClient(), DataType.TYPE_STEP_COUNT_DELTA).await(1, TimeUnit.MINUTES);
        Log.d(TAG, "Data set = " + dataReadResult.getTotal());
        dumpDataSet(dataReadResult.getTotal());
    }


    private void dumpDataSet(DataSet dataSet) {
            int totalSteps = 0;
        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "Data point:");
            Log.i(TAG, "\tType: " + dp.getDataType().getName());
            DateTime startTime = DateTime.forInstant(dp.getStartTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());
            DateTime endTime = DateTime.forInstant(dp.getEndTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());

            Log.i(TAG, "\tStart: " + startTime.format("YYYY-MM-DD hh:mm:ss"));
            Log.i(TAG, "\tEnd: " + endTime.format("YYYY-MM-DD hh:mm:ss"));
            for(Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));
                Log.i(TAG, "\tActivity : " + dp.getValue(field).asActivity());

                totalSteps += dp.getValue(field).asInt();
            }
        }
        Log.i(TAG, "\tTotal Steps: " + totalSteps);

        Intent broadcastDailyStepsIntent = new Intent(Constants.ACTION_DAILY_STEPS);
        broadcastDailyStepsIntent.putExtra(Constants.EXTRA_DAILYSTEPS, totalSteps);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastDailyStepsIntent);

    }

}
