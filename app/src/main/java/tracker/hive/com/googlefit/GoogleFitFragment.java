package tracker.hive.com.googlefit;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.Scopes;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Scope;
import com.google.android.gms.fitness.Fitness;

import tracker.hive.com.FitnessWalkService;
import tracker.hive.com.ReadDailyCaloriesService;
import tracker.hive.com.ReadDailyDistanceService;
import tracker.hive.com.ReadDailySpeedService;
import tracker.hive.com.ReadDailyStepsService;
import tracker.hive.com.helper.GoogleFitManager;
import utils.Utils;

public class GoogleFitFragment extends Fragment {

    private static final int REQUEST_OAUTH = 1;

    /**
     *  Track whether an authorization activity is stacking over the current activity, i.e. when
     *  a known auth error is being resolved, such as showing the account chooser or presenting a
     *  consent dialog. This avoids common duplications as might happen on screen rotations, etc.
     */
    private static final String AUTH_PENDING = "auth_state_pending";
    private boolean authInProgress = false;

    private GoogleApiClient mClient = null;
    private static final String TAG = GoogleFitFragment.class.getSimpleName();

    public GoogleFitFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "Hello Fit");
        if (savedInstanceState != null) {
            authInProgress = savedInstanceState.getBoolean(AUTH_PENDING);
        }


    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        buildFitnessClient();

    }


    @Override
    public void onStart() {
        super.onStart();
        Log.i(TAG, "Connecting...");
        mClient.connect();
    }


    @Override
    public void onStop() {
        super.onStop();
        if (mClient.isConnected()) {
            mClient.disconnect();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_OAUTH) {
            authInProgress = false;
            if (resultCode == Activity.RESULT_OK) {
                // Make sure the app is not already connected or attempting to connect
                if (!mClient.isConnecting() && !mClient.isConnected()) {
                    mClient.connect();
                }
            }
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(AUTH_PENDING, authInProgress);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mClient = null;
        GoogleFitManager.getInstance().clear();
    }

    private void readDailySteps() {

        ReadDailyStepsService.readyDailySteps(getActivity().getApplicationContext());
    }

    private void readDailyDistance() {

        ReadDailyDistanceService.readyDailyDistance(getActivity().getApplicationContext());
    }

    private void readDailySpeed() {

        ReadDailySpeedService.readyDailySpeed(getActivity().getApplicationContext());
    }


    private void startFitnessWalkService() {

        FitnessWalkService.startActionFoo(getActivity().getApplicationContext());
    }

    /**
     *  Build a {@link GoogleApiClient} that will authenticate the user and allow the application
     *  to connect to Fitness APIs. The scopes included should match the scopes your app needs
     *  (see documentation for details). Authentication will occasionally fail intentionally,
     *  and in those cases, there will be a known resolution, which the OnConnectionFailedListener()
     *  can address. Examples of this include the user never having signed in before, or having
     *  multiple accounts on the device and needing to specify which account to use, etc.
     */
    private void buildFitnessClient() {
        // Create the Google API Client
        mClient = new GoogleApiClient.Builder(getActivity())
                .addApi(Fitness.HISTORY_API)
                .addScope(new Scope(Scopes.FITNESS_ACTIVITY_READ))
                .addScope(new Scope(Scopes.FITNESS_LOCATION_READ))
                .addConnectionCallbacks(
                        new GoogleApiClient.ConnectionCallbacks() {

                            @Override
                            public void onConnected(Bundle bundle) {
                                Log.i(TAG, "Connected!!!");
                                GoogleFitManager.getInstance().setGoogleApiClient(mClient);
                                // Now you can make calls to the Fitness APIs.
                                // Put application specific code here.
                                Utils.showToast(getActivity(), "Connected");
                                readDailySteps();
                                readDailyDistance();
//                                readDailySpeed();
//                                readDailySteps();
//                                readDailyStepsWalk();
//                                startFitnessWalkService();
                                readDailyCalories();
                            }

                            @Override
                            public void onConnectionSuspended(int i) {
                                // If your connection to the sensor gets lost at some point,
                                // you'll be able to determine the reason and react to it here.
                                if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_NETWORK_LOST) {
                                    Log.i(TAG, "Connection lost.  Cause: Network Lost.");
                                    Utils.showToast(getActivity(), "Connection lost.  Cause: Network Lost");

                                } else if (i == GoogleApiClient.ConnectionCallbacks.CAUSE_SERVICE_DISCONNECTED) {
                                    Log.i(TAG, "Connection lost.  Reason: Service Disconnected");
                                    Utils.showToast(getActivity(), "Connection lost.  Reason: Service Disconnected");

                                }
                            }
                        }
                )
                .addOnConnectionFailedListener(
                        new GoogleApiClient.OnConnectionFailedListener() {
                            // Called whenever the API client fails to connect.
                            @Override
                            public void onConnectionFailed(ConnectionResult result) {
                                Log.i(TAG, "Connection failed. Cause: " + result.toString());
                                Utils.showToast(getActivity(), "Connection failed. Cause: " + result.toString());

                                if (!result.hasResolution()) {
                                    // Show the localized error dialog
                                    GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(),
                                            getActivity(), 0).show();
                                    return;
                                }
                                // The failure has a resolution. Resolve it.
                                // Called typically when the app is not yet authorized, and an
                                // authorization dialog is displayed to the user.
                                if (!authInProgress) {
                                    try {
                                        Log.i(TAG, "Attempting to resolve failed connection");
                                        Utils.showToast(getActivity(), "Attempting to resolve failed connection: ");

                                        authInProgress = true;
                                        result.startResolutionForResult(getActivity(),
                                                REQUEST_OAUTH);
                                    } catch (IntentSender.SendIntentException e) {
                                        Log.e(TAG,
                                                "Exception while starting resolution activity", e);
                                        Utils.showToast(getActivity(), "Exception while starting resolution activity" );

                                    }
                                }
                            }
                        }
                )
                .build();
    }

    private void readDailyCalories() {

        ReadDailyCaloriesService.ReadDailyCaloriesService(getActivity().getApplicationContext());

    }

}
