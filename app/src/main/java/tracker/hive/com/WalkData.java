package tracker.hive.com;

import java.io.Serializable;

/**
 * Created by sayoojvalsan on 7/17/15.
 */
public class WalkData implements Serializable {

    String mSteps;

    String mMiles;

    String mCalorieBurned;

    public String getSteps() {
        return mSteps;
    }

    public void setSteps(String steps) {
        mSteps = steps;
    }

    public String getMiles() {
        return mMiles;
    }

    public void setMiles(String miles) {
        mMiles = miles;
    }

    public String getCalorieBurned() {
        return mCalorieBurned;
    }

    public void setCalorieBurned(String calorieBurned) {
        mCalorieBurned = calorieBurned;
    }
}
