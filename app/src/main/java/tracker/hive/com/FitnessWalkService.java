package tracker.hive.com;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import tracker.hive.com.helper.StepsHelper;
import tracker.hive.com.helper.WalkDistanceHelper;
import utils.Constants;

public class FitnessWalkService extends IntentService {

    private static final String ACTION_FETCH_WALK_FITNESSDATA = "tracker.hive.com.action.ACTION_FETCH_WALK_FITNESSDATA";
    private static final String TAG = FitnessWalkService.class.getSimpleName();
    private int steps;
    private float distance;

    public static void startActionFoo(Context context) {
        Intent intent = new Intent(context, FitnessWalkService.class);
        intent.setAction(ACTION_FETCH_WALK_FITNESSDATA);
        context.startService(intent);

    }


    public FitnessWalkService() {
        super("FitnessWalkService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FETCH_WALK_FITNESSDATA.equals(action)) {
                handleFetchWalkFitnessData();
            }
        }
    }

    /**
     * Handle action Foo in the provided background thread with the provided
     * parameters.
     */
    private void handleFetchWalkFitnessData() {

        //Get steps
        ExecutorService es = Executors.newFixedThreadPool(3);
        es.execute(new Runnable() {
            @Override
            public void run() {
                steps = new StepsHelper().findSteps();

            }
        });


        es.execute(new Runnable() {
            @Override
            public void run() {
                distance = new WalkDistanceHelper().findDistanceWalked();
            }
        });


        es.shutdown();


        try {
            boolean finshed = es.awaitTermination(30, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "Steps = " + steps);
        Log.d(TAG, "distance = " + distance);
        float caloriesPerMile = (float) (0.53 * 175L);
        float averageStepPermile = (float) (steps / distance);
        float caloriesPerStep = caloriesPerMile / averageStepPermile;
        int caloriesBurned = (int)(caloriesPerStep * steps);
        Log.d(TAG, "caloriesBurned walking = " + caloriesBurned);


        WalkData walkData = new WalkData();
        walkData.setCalorieBurned(String.valueOf(caloriesBurned));
                Intent broadcastDailyStepsIntent = new Intent(Constants.ACTION_DAILY_WALKDATA);
        broadcastDailyStepsIntent.putExtra(Constants.EXTRA_DAILY_WALKDATA, walkData);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastDailyStepsIntent);

    }

}
