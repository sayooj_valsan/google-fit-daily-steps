package tracker.hive.com.demo;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import tracker.hive.com.googlefit.GoogleFitFragment;
import tracker.hive.comgooglefit.R;
import utils.Constants;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = MainActivity.class.getSimpleName();
    private GoogleFitFragment mGoogleFitFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initGoogleFit();
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();

        IntentFilter dailyStepsIntentFilter = new IntentFilter(Constants.ACTION_DAILY_STEPS);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDailyStepsReceiver, dailyStepsIntentFilter);
        IntentFilter dailyDistanceIntentFilter = new IntentFilter(Constants.ACTION_DAILY_DISTANCE);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDailyDistanceReceiver, dailyDistanceIntentFilter );

        IntentFilter walkDataIntentFilter = new IntentFilter(Constants.ACTION_DAILY_CALORIES);
        LocalBroadcastManager.getInstance(this).registerReceiver(mDailyCaloriesReceiver, walkDataIntentFilter );


    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDailyStepsReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mDailyDistanceReceiver);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initGoogleFit() {

        FragmentManager fragmentManager = getSupportFragmentManager();
        //Non UI fragment
        mGoogleFitFragment = (GoogleFitFragment)fragmentManager.findFragmentByTag(Constants.FRAGMENT_GOOGLE_FIT);
        if (mGoogleFitFragment == null) {
            mGoogleFitFragment = new GoogleFitFragment();
            // Tell it who it is working with.
            fragmentManager.beginTransaction().add(mGoogleFitFragment, Constants.FRAGMENT_GOOGLE_FIT).commit();
        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mGoogleFitFragment != null){
            mGoogleFitFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private BroadcastReceiver mDailyStepsReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int totalSteps =  intent.getIntExtra(Constants.EXTRA_DAILYSTEPS, -1);
            Log.d(TAG, "Received totalSteps " + totalSteps);
            ((TextView)findViewById(R.id.tvSteps)).setText(String.valueOf(totalSteps));

        }
    };

    private BroadcastReceiver mDailyDistanceReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            float totalDistance =  intent.getFloatExtra(Constants.EXTRA_DAILYDISTANCE, -1f);
            Log.d(TAG, "Received totalDistance " + totalDistance);
            ((TextView)findViewById(R.id.tvDistance)).setText(String.valueOf((Math.round(totalDistance * 100.0)/100.0)));

        }
    };

    private BroadcastReceiver mDailyCaloriesReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            int calories = Math.round(intent.getFloatExtra(Constants.EXTRA_DAILY_CALORIES, 0L));
            Log.d(TAG, "Received Calories burned " + calories);
            ((TextView)findViewById(R.id.tvCaloriesBurned)).setText(String.valueOf(calories));

        }
    };
}
