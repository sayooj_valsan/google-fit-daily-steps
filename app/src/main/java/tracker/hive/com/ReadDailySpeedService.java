package tracker.hive.com;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import org.intelligentsia.hirondelle.date4j.DateTime;

import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import tracker.hive.com.helper.GoogleFitManager;
import utils.Constants;

/**
 * Responsible for reading daily distance
 */
public class ReadDailySpeedService extends IntentService {
    private static final String ACTION_READ_DAILY_SPEED = "ACTION_READ_DAILY_SPEED";
    private static final String TAG = ReadDailySpeedService.class.getSimpleName();

    public static void readyDailySpeed(Context context) {
        Intent intent = new Intent(context, ReadDailySpeedService.class);
        intent.setAction(ACTION_READ_DAILY_SPEED);
        context.startService(intent);
    }

    public ReadDailySpeedService() {
        super("ReadDailyDistanceService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_READ_DAILY_SPEED.equals(action)) {
                readDailyWalkDuration();
            }
        }
    }

    private List<String> getWalkingActivities() {
        return Arrays.asList(
                FitnessActivities.WALKING,
                FitnessActivities.WALKING_FITNESS,
                FitnessActivities.WALKING_NORDIC,
                FitnessActivities.WALKING_TREADMILL);
    }

    private void readDailyWalkDuration() {


        if(GoogleFitManager.getInstance().getGoogleApiClient() == null){
            return;
        }
        DateTime now = DateTime.now(TimeZone.getDefault());
        DateTime morning = now.getStartOfDay();
        DateTime midnight = now.plusDays(1).getStartOfDay();
        PendingResult<DataReadResult> pendingResult = Fitness.HistoryApi.readData(
                GoogleFitManager.getInstance().getGoogleApiClient(),
                new DataReadRequest.Builder()
                        .aggregate(DataType.TYPE_STEP_COUNT_DELTA, DataType.AGGREGATE_STEP_COUNT_DELTA)
                        .bucketByActivityType(1, TimeUnit.SECONDS)
                        .setTimeRange(morning.getMilliseconds(TimeZone.getDefault()), midnight.getMilliseconds(TimeZone.getDefault()), TimeUnit.MILLISECONDS)
                        .build());
        DataReadResult readDataResult = pendingResult.await();

        for (Bucket activityBucket : readDataResult.getBuckets()) {
            for (DataSet activityDataSet : activityBucket.getDataSets()) {
                dumpDataSet(activityDataSet);
            }
        }

    }


    private void dumpDataSet(DataSet dataSet) {
        float totalSpeed = 0;
        float count = 0f;
        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "Data point:");
            Log.i(TAG, "\tType: " + dp.getDataType().getName());
            DateTime startTime = DateTime.forInstant(dp.getStartTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());
            DateTime endTime = DateTime.forInstant(dp.getEndTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());

            Log.i(TAG, "\tStart: " + startTime.format("YYYY-MM-DD hh:mm:ss"));
            Log.i(TAG, "\tEnd: " + endTime.format("YYYY-MM-DD hh:mm:ss"));
            for(Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));

                totalSpeed += Float.parseFloat(dp.getValue(field).toString());
                count++;
            }
        }
        //Log.i(TAG, "\tAverage speed m/h " + totalSpeed/count);

        Intent broadcastDailyStepsIntent = new Intent(Constants.ACTION_DAILY_SPEED);
        broadcastDailyStepsIntent.putExtra(Constants.ACTION_DAILY_SPEED, totalSpeed/count);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastDailyStepsIntent);

    }

}
