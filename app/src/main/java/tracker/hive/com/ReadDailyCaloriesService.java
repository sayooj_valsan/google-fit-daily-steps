package tracker.hive.com;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import org.intelligentsia.hirondelle.date4j.DateTime;

import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import tracker.hive.com.helper.GoogleFitManager;
import utils.Constants;

/**
 * Responsible for reading daily distance
 */
public class ReadDailyCaloriesService extends IntentService {
    private static final String ACTION_READ_DAILY_CALORIES = "ACTION_READ_DAILY_CALORIES";
    private static final String TAG = ReadDailyCaloriesService.class.getSimpleName();

    public static void ReadDailyCaloriesService(Context context) {
        Intent intent = new Intent(context, ReadDailyCaloriesService.class);
        intent.setAction(ACTION_READ_DAILY_CALORIES);
        context.startService(intent);
    }

    public ReadDailyCaloriesService() {
        super("ReadDailyDistanceService");

    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_READ_DAILY_CALORIES.equals(action)) {
                readDailyCalories();
            }
        }
    }

    private List<String> getWalkingActivities() {
        return Arrays.asList(
                FitnessActivities.WALKING,
                FitnessActivities.WALKING_FITNESS,
                FitnessActivities.WALKING_NORDIC,
                FitnessActivities.WALKING_TREADMILL);
    }

    private void readDailyCalories() {

        List<String> walkingActivities;

        walkingActivities = getWalkingActivities();


        if(GoogleFitManager.getInstance().getGoogleApiClient() == null){
            return;
        }
        DateTime now = DateTime.now(TimeZone.getDefault());
        DateTime morning = now.getStartOfDay();
        DateTime midnight = now.plusDays(1).getStartOfDay();
        float totalCalories = 0;

        PendingResult<DataReadResult> pendingResult = Fitness.HistoryApi.readData(
                GoogleFitManager.getInstance().getGoogleApiClient(),
                new DataReadRequest.Builder()
                        .aggregate(DataType.TYPE_CALORIES_EXPENDED, DataType.AGGREGATE_CALORIES_EXPENDED)
                        .bucketByActivityType(1, TimeUnit.SECONDS)
                        .setTimeRange(morning.getMilliseconds(TimeZone.getDefault()), midnight.getMilliseconds(TimeZone.getDefault()), TimeUnit.MILLISECONDS)
                        .build());
        DataReadResult readDataResult = pendingResult.await();
//        DataSet dataSet = readDataResult.getDataSet(DataType.TYPE_DISTANCE_DELTA);
//        Log.d(TAG, "Data set = " + dataSet);
        for (Bucket activityBucket : readDataResult.getBuckets()) {
            String activity = activityBucket.getActivity();
            Log.d(TAG, "Activity : " + activity);
            for (DataSet activityDataSet : activityBucket.getDataSets()) {

                for (DataPoint activityDataPoint : activityDataSet.getDataPoints()) {
                    dumpDataSet(activityDataSet);
                    float calories = activityDataPoint.getValue(Field.FIELD_CALORIES).asFloat();

                    if (walkingActivities.contains(activity)) {
                        totalCalories +=  calories;
                        continue;
                    }

//                    if (runningActivities.contains(activity)) {
//                        activityDistances.getRunningDistance().plus(activityDistance);
//                        continue;
//                    }
//
//                    if (bikingActivities.contains(activity)) {
//                        activityDistances.getBikingDistance().plus(activityDistance);
//                        continue;
//                    }
                }
            }
        }

        Log.i(TAG, "\tTotal Calories  " + totalCalories);
//
        Intent broadcastDailyStepsIntent = new Intent(Constants.ACTION_DAILY_CALORIES);
        broadcastDailyStepsIntent.putExtra(Constants.EXTRA_DAILY_CALORIES, totalCalories);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastDailyStepsIntent);


    }

    private void dumpDataSet(DataSet dataSet) {
        float totalMeters = 0;
        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "Data point:");
            Log.i(TAG, "\tType: " + dp.getDataType().getName());
            DateTime startTime = DateTime.forInstant(dp.getStartTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());
            DateTime endTime = DateTime.forInstant(dp.getEndTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());

            Log.i(TAG, "\tStart: " + startTime.format("YYYY-MM-DD hh:mm:ss"));
            Log.i(TAG, "\tEnd: " + endTime.format("YYYY-MM-DD hh:mm:ss"));
            for(Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));

               // Log.i(TAG, "\tActivity : " + dp.getValue(field).asActivity());

                totalMeters += Float.parseFloat(dp.getValue(field).toString());
            }
        }
        Log.i(TAG, "\tTotal calories in meters " + totalMeters);
//
//        Intent broadcastDailyStepsIntent = new Intent(Constants.ACTION_DAILY_DISTANCE);
//        broadcastDailyStepsIntent.putExtra(Constants.EXTRA_DAILYDISTANCE, totalMeters * 0.00062137f);
//        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastDailyStepsIntent);

    }

}
