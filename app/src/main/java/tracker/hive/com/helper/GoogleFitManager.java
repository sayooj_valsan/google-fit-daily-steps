package tracker.hive.com.helper;

import com.google.android.gms.common.api.GoogleApiClient;

/**
 * Created by sayoojvalsan on 6/12/15.
 */
public class GoogleFitManager {
    private static GoogleFitManager ourInstance = new GoogleFitManager();
    private GoogleApiClient mGoogleApiClient;

    public static GoogleFitManager getInstance() {
        return ourInstance;
    }

    private GoogleFitManager() {
    }

    public void  setGoogleApiClient(GoogleApiClient googleApiClient){
        mGoogleApiClient = googleApiClient;
    }
    public GoogleApiClient  getGoogleApiClient(){
        return mGoogleApiClient;
    }

    public void clear() {
        mGoogleApiClient = null;
    }
}
