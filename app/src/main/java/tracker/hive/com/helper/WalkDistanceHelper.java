package tracker.hive.com.helper;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessActivities;
import com.google.android.gms.fitness.data.Bucket;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.request.DataReadRequest;
import com.google.android.gms.fitness.result.DataReadResult;

import org.intelligentsia.hirondelle.date4j.DateTime;

import java.util.Arrays;
import java.util.List;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import utils.Constants;

/**
 * Created by sayoojvalsan on 7/17/15.
 */
public class WalkDistanceHelper {

    private static final String TAG = WalkDistanceHelper.class.getSimpleName();

    public float findDistanceWalked(){

    return  readDailyDistance();
    }

    private List<String> getWalkingActivities() {
        return Arrays.asList(
                FitnessActivities.WALKING,
                FitnessActivities.WALKING_FITNESS,
                FitnessActivities.WALKING_NORDIC,
                FitnessActivities.WALKING_TREADMILL);
    }

    private float  readDailyDistance() {

        List<String> walkingActivities;

        walkingActivities = getWalkingActivities();


        if(GoogleFitManager.getInstance().getGoogleApiClient() == null){
            return 0;
        }
        DateTime now = DateTime.now(TimeZone.getDefault());
        DateTime morning = now.getStartOfDay();
        DateTime midnight = now.plusDays(1).getStartOfDay();
        float totalMeters = 0;

        PendingResult<DataReadResult> pendingResult = Fitness.HistoryApi.readData(
                GoogleFitManager.getInstance().getGoogleApiClient(),
                new DataReadRequest.Builder()
                        .aggregate(DataType.TYPE_DISTANCE_DELTA, DataType.AGGREGATE_DISTANCE_DELTA)
                        .bucketByActivityType(1, TimeUnit.SECONDS)
                        .setTimeRange(morning.getMilliseconds(TimeZone.getDefault()), midnight.getMilliseconds(TimeZone.getDefault()), TimeUnit.MILLISECONDS)
                        .build());
        DataReadResult readDataResult = pendingResult.await();
//        DataSet dataSet = readDataResult.getDataSet(DataType.TYPE_DISTANCE_DELTA);
//        Log.d(TAG, "Data set = " + dataSet);
        for (Bucket activityBucket : readDataResult.getBuckets()) {
            String activity = activityBucket.getActivity();

            for (DataSet activityDataSet : activityBucket.getDataSets()) {

                for (DataPoint activityDataPoint : activityDataSet.getDataPoints()) {
                    //dumpDataSet(activityDataSet);
                    float activityDistance = activityDataPoint.getValue(Field.FIELD_DISTANCE).asFloat();

                    if (walkingActivities.contains(activity)) {
                        totalMeters +=  activityDistance;
                        continue;
                    }

//                    if (runningActivities.contains(activity)) {
//                        activityDistances.getRunningDistance().plus(activityDistance);
//                        continue;
//                    }
//
//                    if (bikingActivities.contains(activity)) {
//                        activityDistances.getBikingDistance().plus(activityDistance);
//                        continue;
//                    }
                }
            }
        }

        Log.i(TAG, "\tTotal Distance in meters " + totalMeters);
//
        return totalMeters * 0.00062137f;

    }

    private float dumpDataSet(DataSet dataSet) {
        int totalMeters = 0;
        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "Data point:");
            Log.i(TAG, "\tType: " + dp.getDataType().getName());
            DateTime startTime = DateTime.forInstant(dp.getStartTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());
            DateTime endTime = DateTime.forInstant(dp.getEndTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());

            Log.i(TAG, "\tStart: " + startTime.format("YYYY-MM-DD hh:mm:ss"));
            Log.i(TAG, "\tEnd: " + endTime.format("YYYY-MM-DD hh:mm:ss"));
            for(Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));

                // Log.i(TAG, "\tActivity : " + dp.getValue(field).asActivity());

                totalMeters += Float.parseFloat(dp.getValue(field).toString());
            }
        }
        Log.i(TAG, "\tTotal Distance in meters " + totalMeters);
//
        return totalMeters *  0.00062137f;
    }
}
