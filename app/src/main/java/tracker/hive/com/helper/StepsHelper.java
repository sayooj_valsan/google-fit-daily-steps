package tracker.hive.com.helper;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataSet;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DailyTotalResult;

import org.intelligentsia.hirondelle.date4j.DateTime;

import java.util.TimeZone;
import java.util.concurrent.TimeUnit;


/**
 * Created by sayoojvalsan on 7/17/15.
 */
public class StepsHelper {

    private static final String TAG = StepsHelper.class.getSimpleName();

    public int findSteps()
    {


        return readDailySteps();
    }

    private int readDailySteps() {
        if(GoogleFitManager.getInstance().getGoogleApiClient() == null){
            return 0 ;
        }
        DailyTotalResult dataReadResult =
                Fitness.HistoryApi.readDailyTotal(GoogleFitManager.getInstance().getGoogleApiClient(), DataType.TYPE_STEP_COUNT_DELTA).await(1, TimeUnit.MINUTES);
        Log.d(TAG, "Data set = " + dataReadResult.getTotal());
        return dumpDataSet(dataReadResult.getTotal());
    }


    private int dumpDataSet(DataSet dataSet) {
        int totalSteps = 0;
        for (DataPoint dp : dataSet.getDataPoints()) {
            Log.i(TAG, "Data point:");
            Log.i(TAG, "\tType: " + dp.getDataType().getName());
            DateTime startTime = DateTime.forInstant(dp.getStartTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());
            DateTime endTime = DateTime.forInstant(dp.getEndTime(TimeUnit.MILLISECONDS), TimeZone.getDefault());

            Log.i(TAG, "\tStart: " + startTime.format("YYYY-MM-DD hh:mm:ss"));
            Log.i(TAG, "\tEnd: " + endTime.format("YYYY-MM-DD hh:mm:ss"));
            for(Field field : dp.getDataType().getFields()) {
                Log.i(TAG, "\tField: " + field.getName() +
                        " Value: " + dp.getValue(field));
                Log.i(TAG, "\tActivity : " + dp.getValue(field).asActivity());

                totalSteps += dp.getValue(field).asInt();
            }
        }
        Log.i(TAG, "\tTotal Steps: " + totalSteps);

        return totalSteps;
    }
}
