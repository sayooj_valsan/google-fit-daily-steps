package utils;

/**
 * Created by sayoojvalsan on 6/12/15.
 */
public class Constants {


    public static final String FRAGMENT_GOOGLE_FIT = "FRAGMENT_GOOGLE_FIT" ;
    public static final String EXTRA_DAILYDISTANCE =  "EXTRA_DAILYDISTANCE";
    public static final String EXTRA_DAILY_WALKDATA =  "EXTRA_DAILWALKDATA";
    public static final String EXTRA_DAILY_CALORIES =  "EXTRA_DAILY_CALORIES";

    public static final String EXTRA_DAILYSTEPS =  "EXTRA_DAILYSTEPS";
    public static final String ACTION_DAILY_STEPS = "ACTION_DAILY_STEPS" ;
    public static final String ACTION_DAILY_DISTANCE = "ACTION_DAILY_DISTANCE" ;
    public static final String ACTION_DAILY_SPEED = "ACTION_DAILY_SPEED" ;
    public static final String ACTION_DAILY_WALKDATA = "DAILYWALKDATA" ;
    public static final String ACTION_DAILY_CALORIES = "ACTION_DAILY_CALORIES" ;

}
